<?php
return array(
    'zh-cn' => array(
        '[DATA_TO_DELETE_CAN_NOT_NULL]' => '需要删除的删除键不能为空',
        '[NO_RIGHT_TO_OPERATING_THE_DATA]' => '没有操作数据的权限',

        '[USER_LOGIN_PARAM_ERROR]' => '账号或密码错误，请重新登录',
        '[USER_LOGIN_RAND_CODE_ERROR]' => '登录验证码错误',
        '[USER_LOGIN_ERROR_NOT_ALLOWED]' => '登录错误：账号未审核',
        '[USER_LOGIN_ERROR_NOT_PASS]' => '登录错误：审核未通过',
        '[USER_LOGIN_ERROR_ACCOUNT_STOPPED]' => '登录错误：账号已被禁止',

        '[USER_LOGIN_SUCCESS]' => '用户%s登录成功',
        '[USER_NOT_LOGIN]' => '检测到您还未登录，请先登录，再进行此操作',
        '[USER_ACCESS_FORBIDDEN]' => '您没有权限访问此功能',
        '[FORBIDDEN]' => '您没有权限访问此功能',
        '[ACCOUNT_EXIST]' => '相同的账号已存在',
        '[PHONE_EXIST]' => '相同的手机号已存在',
        '[USER_MANAGE_ADD_USER]' => '添加用户，用户信息为：%s',
        '[USER_NOT_EXIST]' => '用户不存在',
        '[USER_MANAGE_EDIT_USER]' => '修改用户，用户账号为：%s，修改为：%s',
        '[USER_MANAGE_DELETE_USER]' => '删除用户，用户账号为：%s，账号数据：%s',
        '[CAN_NOT_ACCESS_OTHERS]' => '不能修改他人账号的信息',
        '[OLD_PASSWORD_ERROR]' => '旧密码不正确',
        '[USER_MODIFY_INFO]' => '用户修改个人信息，账号为：%s，修改为：%s',
        '[CAN_NOT_DELETE_SELF]' => '不能删除当前登录账号',

        '[DOMAIN_TO_OPERATION_CAN_NOT_NULL]' => '需要批量操作的域名不能为空',
        '[DOMAIN_TO_DELETE_CAN_NOT_NULL]' => '需要删除的域名不能为空',
        '[DOMAIN_EXISTS]' => '已存在相同的域名',
        '[DOMAIN_NOT_EXISTS]' => '域名不存在',
        '[DOMAIN_PARENT_EXISTS]' => '此域名的父级域名已存在，请在父级域名中直接添加该域名解析记录',
        '[DOMAIN_OVER_MAX_ALLOWED]' => '域名已超出最大允许数量',
        '[DOMAIN_RECORD_NOT_EXISTS]' => '域名记录不存在',

        '[RECORD_NOT_FOUND]' => '解析记录不存在',
        '[RECORD_DATA_NOT_NULL]' => '记录值不能为空',
        '[RECORD_DATA_MUST_IPV4]' => 'A记录值必须为IPV4地址',
        '[RECORD_DATA_MUST_IPV6]' => 'AAAA记录值必须为IPV6地址',
        '[RECORD_DATA_MUST_DOMAIN]' => 'MX或CNAME记录值必须为域名',
        '[RECORD_DATA_MUST_SRV]' => 'SRV记录值格式不正确',
        '[RECORD_SAME_EXISTS]' => '已存在相同的解析记录',
        '[RECORD_SAME_URL_EXISTS]' => '已存在相同主机头和线路的URL转发',

        '[URL_SAME_EXISTS]' => '已存在相同的转发记录',

        '[RECORD_BALANCE_AUX_NOT_NULL]' => '优先级设置不正确',

        '[NET_NAME_EXIST]' => '已存在相同的线路名',
        '[NET_NOT_EXISTS]' => '该网络组（解析线路）不存在',

        '[IP_BATCH_TXT_ERROR]' => '批量操作IP内容有不符合要求的行',

        '[HOST_IP_NOT_SAME]' => '需要修改的IP不正确',

        '[INSTALL_ERROR_INSTALLED]' => '系统已进行过安装，请勿重复操作',
        '[INSTALL_ERROR_ON_TABLE]' => '在创建表%s时发生错误：%s',
        '[INSTALL_ERROR_WRITE_CONFIG]' => '写入配置记录文件失败',
        '[INSTALL_ERROR_WRITE_LOCK]' => '写入安装记录文件失败',

        '[HTTP_API_CLOSED]' => 'Http API已关闭',
        '[HTTP_API_ACCOUNT_ERROR]' => '接口账号信息错误',
        '[IFRAME_API_CLOSED]' => 'IFrame API已关闭',
        '[IFRAME_API_TOKEN_ERROR]' => 'IFrame API Token 错误',
        '[IFRAME_API_TOKEN_EXPIRED]' => 'IFrame API Token 已过期'

    )
);