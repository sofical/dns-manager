<?php
namespace com\rs\dns\constant;

/**
 * Class RecordUrlConst
 * @package com\rs\dns\constant
 */
final class RecordUrlConst {
    const URL_SAME_EXISTS = 'URL_SAME_EXISTS';
    const URL_DATA_NOT_EXISTS = 'URL_DATA_NOT_EXISTS';
}