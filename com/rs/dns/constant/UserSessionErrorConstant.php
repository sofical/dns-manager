<?php
namespace com\rs\dns\constant;

class UserSessionErrorConstant {
    /**
     * 登录异常.
     */
    const USER_LOGIN_PARAM_ERROR = '[USER_LOGIN_PARAM_ERROR]';
    const USER_LOGIN_PARAM_ERROR_CODE = 'BusinessAssistant/USER_LOGIN_PARAM_ERROR';
    const USER_NOT_LOGIN = 'BusinessAssistant/USER_NOT_LOGIN';
    const USER_ACCESS_FORBIDDEN = 'BusinessAssistant/USER_ACCESS_FORBIDDEN';

    /**
     * 登录验证码错误.
     */
    const USER_LOGIN_RAND_CODE_ERROR = 'USER_LOGIN_RAND_CODE_ERROR';

    /**
     * 登录错误：账号未审核.
     */
    const USER_LOGIN_ERROR_NOT_ALLOWED = 'USER_LOGIN_ERROR_NOT_ALLOWED';

    /**
     * 登录错误：审核未通过.
     */
    const USER_LOGIN_ERROR_NOT_PASS = 'USER_LOGIN_ERROR_NOT_PASS';

    /**
     * 登录错误：账号已被禁止.
     */
    const USER_LOGIN_ERROR_ACCOUNT_STOPPED = 'USER_LOGIN_ERROR_ACCOUNT_STOPPED';
}