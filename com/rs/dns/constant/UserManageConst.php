<?php
namespace com\rs\dns\constant;

/**
 * Class UserManageConst
 * @package php\constant
 */
final class UserManageConst {
    const ACCOUNT_EXIST = "[ACCOUNT_EXIST]";
    const ACCOUNT_EXIST_CODE = "BusinessAssistant/ACCOUNT_EXIST";
    const PHONE_EXIST = "[PHONE_EXIST]";
    const PHONE_EXIST_CODE = "BusinessAssistant/PHONE_EXIST";
    const USER_NOT_EXIST = "[USER_NOT_EXIST]";
    const USER_NOT_EXIST_CODE = "BusinessAssistant/USER_NOT_EXIST";
    const CAN_NOT_DELETE_SELF = "CAN_NOT_DELETE_SELF";
}