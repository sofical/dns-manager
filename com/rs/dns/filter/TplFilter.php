<?php
namespace com\rs\dns\filter;

use restphp\http\RestHttpRequest;
use restphp\tpl\RestTpl;
use restphp\utils\RestHttpRequestUtils;
use restphp\utils\RestStringUtils;

final class TplFilter {
    private static $_map = array(
        '/css/',
        '/js/',
        '/images/'
    );

    private static $_extMap = array(
        'js' => 'application/javascript',
        'css' => 'text/css'
    );

    public static function run() {
        $strUri = RestHttpRequestUtils::getUri();
        $arrUri = explode("?", $strUri);
        $strUri = $arrUri[0];

        foreach (self::$_map as $match) {
            if (RestStringUtils::startWith($strUri, $match)) {
                $ext = substr($strUri, strrpos($strUri, ".") + 1);
                if (isset(self::$_extMap[$ext])) {
                    $mime = self::$_extMap[$ext];
                } else {
                    $mime = mime_content_type(substr(RestTpl::getTplRoot(), 1) . $strUri);
                }
                header('Content-Type:' . $mime);

                $url = RestHttpRequestUtils::getScheme() . "://" .RestHttpRequestUtils::getHost() .
                    RestTpl::getTplRoot() . $strUri;
                echo file_get_contents($url);
                die();
            }
        }
    }
}