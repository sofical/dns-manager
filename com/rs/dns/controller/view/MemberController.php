<?php
namespace com\rs\dns\controller\view;

use com\rs\dns\controller\BaseController;

/**
 * Class MemberController
 * @package com\rs\dns\controller\view
 * @RequestMapping("")
 */
final class MemberController extends BaseController {
    /**
     * 会员中心-首页.
     * @RequestMapping(value="/member-config", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function memberConfig() {
        $this->_tpl->display('bm-member-config.tpl');
    }

    /**
     * 会员中心-域名管理.
     * @RequestMapping(value="/member-domains", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function memberDomains() {
        $this->_tpl->display('bm-member-domains.tpl');
    }

    /**
     * 会员中心-解析记录.
     * @RequestMapping(value="/member-records", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function memberRecords() {
        $this->_tpl->display('bm-member-records.tpl');
    }

    /**
     * 会员中心-DNS解析.
     * @RequestMapping(value="/member-records-dns", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function memberRecordsDns() {
        $this->_tpl->display('bm-member-records-dns.tpl');
    }

    /**
     * 会员中心-DNS解析-负载均衡.
     * @RequestMapping(value="/member-records-balance", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function memberRecordsBalance() {
        $this->_tpl->display('bm-member-records-balance.tpl');
    }

    /**
     * 会员中心-解析-转发.
     * @RequestMapping(value="/member-records-url", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function memberRecordsUrl() {
        $this->_tpl->display('bm-member-records-url.tpl');
    }

    /**
     * 会员中心-个人信息修改.
     * @RequestMapping(value="/member-info", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function memberInfo() {
        $this->_tpl->display('bm-member-info.tpl');
    }
}