<?php
namespace com\rs\dns\controller;

use com\rs\dns\constant\LangConst;
use com\rs\dns\constant\UserSessionErrorConstant;
use com\rs\dns\filter\IFrameApiFilter;
use com\rs\dns\service\user\impl\UserSessionServiceImpl;
use com\rs\dns\service\user\UserSessionService;
use restphp\exception\RestException;
use restphp\http\RestHttpResponse;
use restphp\http\RestHttpStatus;
use restphp\tpl\RestTpl;
use restphp\utils\RestHttpRequestUtils;
use restphp\utils\RestStringUtils;

/**
 * Class BaseController
 * @package classess\controller\view
 */
class BaseController {
    /**
     * @var RestTpl 模板引擎
     */
    protected $_tpl;

    /**
     * 用户
     * @var UserSessionService user session interface.
     */
    protected $_userSession;

    public function __construct()
    {
        $this->_tpl = new RestTpl();
        $this->_tpl->caching = false;
        $this->_tpl->assign('tpl', $this->_tpl);
        $this->_tpl->assign('isIFrame', IFrameApiFilter::isIFrameApi() ? 'Y' : 'N');
        $this->_tpl->assign('iFrameToken', IFrameApiFilter::getToken());

        $this->_userSession = new UserSessionServiceImpl();

        //权限检查
        $this->_notLoginAlert();
    }

    /**
     * 不需要登录即可访问的地址.
     * @var array
     */
    private $_arrWithoutSessionUri = array(
        '/',
        '/index.html',
        '/undefined',
        '/members.html',
        '/member-register.html',
        '/members/randcode',
        '/install',
        '/install/env',
        '/install/make'
    );

    private $_arrWithoutSessionStartUri = array(
        '/api/user/session',
        '/api/users',
        '/notice-info',
        '/api/news'
    );

    private $_adminUri = array(
        '/manage',
        '/api/manage'
    );

    private $_supperUri = array(
        '/super'
    );

    /**
     * 检查普通登录..
     * @throws RestException
     */
    private function _notLoginAlert() {

        $strUri = RestHttpRequestUtils::getUri();
        if (in_array($strUri, $this->_arrWithoutSessionUri)) {
            return;
        }

        foreach ($this->_arrWithoutSessionStartUri as $startUri) {
            if (RestStringUtils::startWith($strUri, $startUri)) {
                return;
            }
        }

        if ($this->_userSession->isLogin()) {
            //商家权限处理
            if ($this->_isAdminUri($strUri) && !$this->_userSession->isAdmin() && !$this->_userSession->isSuper()) {
                throw new RestException(LangConst::USER_ACCESS_FORBIDDEN, UserSessionErrorConstant::USER_ACCESS_FORBIDDEN, RestHttpStatus::Forbidden);
            }

            //超管理权限处理
            if ($this->_isSuper($strUri) && !$this->_userSession->isSuper()) {
                throw new RestException(LangConst::USER_ACCESS_FORBIDDEN, UserSessionErrorConstant::USER_ACCESS_FORBIDDEN, RestHttpStatus::Forbidden);
            }

            return;
        }
        header("Location: /members.html");
    }

    /**
     * 是否为商家享有的地址.
     * @param $strUri string
     * @return bool
     */
    private function _isAdminUri($strUri) {
        foreach ($this->_adminUri as $adminUri) {
            if (RestStringUtils::startWith($strUri, $adminUri)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否为超管享有的地址.
     * @param $strUri string.
     * @return bool
     */
    private function _isSuper($strUri) {
        foreach ($this->_supperUri as $superUri) {
            if (RestStringUtils::startWith($strUri, $superUri)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 返回成功报文.
     * @param $object
     */
    protected function _success($object) {
        RestHttpResponse::json($object);
    }

    /**
     * 当前登录用户ID.
     * @return mixed
     */
    protected function _getUserId() {
        return $this->_userSession->getSession()->getID();
    }

    /**
     * 当前登录用户.
     * @return mixed
     */
    protected function _getUsername() {
        return $this->_userSession->getSession()->getUsername();
    }
}