<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\controller\api\vo\CommonQuery;
use com\rs\dns\controller\api\vo\NetForm;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\NetService;
use restphp\http\RestHttpRequest;

/**
 * Class ManageNetController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/manage/nets")
 */
final class ManageNetController extends BaseController {
    /**
     * 获取线路列表.
     * @RequestMapping(value="", method="GET")
     */
    public function getNetList() {
        $pageParam = RestHttpRequest::getPageParam();
        $commonQuery = RestHttpRequest::getParameterAsObject(new CommonQuery());
        $netList = NetService::getNetList($commonQuery, $pageParam);
        $this->_success($netList);
    }

    /**
     * 添加线路.
     * @RequestMapping(value="", method="POST")
     * @throws \com\rs\dns\exception\BaException
     */
    public function addNet() {
        $netForm = RestHttpRequest::getBody(new NetForm(), true);
        NetService::add($netForm);
    }

    /**
     * 修改线路.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws \com\rs\dns\exception\BaException
     */
    public function modifyNet() {
        $id = RestHttpRequest::getPathValue("id");
        $netForm = RestHttpRequest::getRequestBody(new NetForm(), true);
        NetService::modify($id, $netForm);
    }

    /**
     * 删除线路.
     * @RequestMapping(value="/{id}", method="DELETE")
     */
    public function deleteNet() {
        $id = RestHttpRequest::getPathValue("id");
        NetService::deleteBatch(array($id));
    }

    /**
     * 批量删除.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteBatch() {
        $arrId = RestHttpRequest::getRequestBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(CommonConstant::DATA_TO_DELETE_CAN_NOT_NULL);
        }
        NetService::deleteBatch($arrId);
    }

    /**
     * 获取所有可用网络.
     * @RequestMapping(value="/action/collect", method="GET")
     */
    public function getCollect() {
        $collect = NetService::getCollect();
        $this->_success($collect);
    }
}