<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\controller\api\vo\RecordBalanceForm;
use com\rs\dns\controller\BaseController;
use com\rs\dns\service\RecordService;
use restphp\http\RestHttpRequest;

/**
 * Class ManageRecordBalanceController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/manage/records")
 */
final class ManageRecordBalanceController extends BaseController {
    /**
     * 获取列表.
     * @RequestMapping("/{recordId}/balances", method="GET")
     */
    public function getList() {
        $recordId = RestHttpRequest::getPathValue("recordId");
        $arrList = RecordService::getBalanceList($recordId);
        $this->_success($arrList);
    }

    /**
     * 设置负载均衡.
     * @RequestMapping("/{recordId}/balances", method="POST")
     */
    public function setBalance() {
        $balanceForm = RestHttpRequest::getRequestBody(new RecordBalanceForm());
        RecordService::setBalance($balanceForm);
    }
}