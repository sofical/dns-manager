<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\controller\api\vo\RecordForm;
use com\rs\dns\controller\api\vo\RecordQuery;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\filter\ApiFilter;
use com\rs\dns\service\DomainService;
use com\rs\dns\service\RecordService;
use restphp\http\RestHttpRequest;
use restphp\http\RestHttpStatus;


/**
 * Class ManageRecordController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/manage/records")
 */
final class ManageRecordController extends BaseController {
    /**
     * 域名管理列表.
     * @RequestMapping(value="", method="GET")
     */
    public function recordList() {
        $pageParam = RestHttpRequest::getPageParam();
        $recordQuery = RestHttpRequest::getParameterAsObject(new RecordQuery());
        $domainList = RecordService::getRecordList($recordQuery, $pageParam);
        $this->_success($domainList);
    }

    /**
     * 新增域名.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     */
    public function add() {
        $recordForm = RestHttpRequest::getRequestBody(new RecordForm(), false);
        if (!DomainService::hadDomain($recordForm->getOrigin(), $this->_getUsername())) {
            ApiFilter::autoCreateDomain($this->_getUsername(), $recordForm->getOrigin());
        }
        RestHttpRequest::getRequestBody(new RecordForm(), true);
        RecordService::add($recordForm);
    }

    /**
     * 修改域名信息.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws BaException
     */
    public function modify() {
        $recordForm = RestHttpRequest::getRequestBody(new RecordForm(), true);
        $id = RestHttpRequest::getPathValue("id");
        RecordService::modify($id, $recordForm);
    }

    /**
     * 单个删除域名.
     * @RequestMapping(value="/{id}", method="DELETE")
     */
    public function delete() {
        $id = RestHttpRequest::getPathValue("id");
        RecordService::delete($id);
    }

    /**
     * 批量删除域名.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteDomainBatch() {
        $arrId = RestHttpRequest::getRequestBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(CommonConstant::DATA_TO_DELETE_CAN_NOT_NULL);
        }
        RecordService::batchDelete($arrId);
    }
}