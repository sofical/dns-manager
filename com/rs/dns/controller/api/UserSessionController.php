<?php
/**
 * Created by zj.
 * User: zj
 * Date: 2019/11/14 0014
 * Time: 下午 11:39
 */
namespace com\rs\dns\controller\api;
use com\rs\dns\constant\LangConst;
use com\rs\dns\constant\UserSessionErrorConstant;
use com\rs\dns\controller\api\vo\UserLoginForm;
use com\rs\dns\controller\BaseController;
use restphp\exception\RestException;
use restphp\http\RestHttpRequest;
use restphp\http\RestHttpResponse;
use restphp\http\RestHttpStatus;

/**
 * Class UserSessionController
 * @RequestMapping("/api/user/session")
 * @package php\controller\user
 */
class UserSessionController extends BaseController {

    /**
     * 登录.
     * @RequestMapping(value="", method="POST")
     * @throws RestException
     */
    public function makeSession() {
        $_userForm = RestHttpRequest::getRequestBody(new UserLoginForm());

        $userBean = $this->_userSession->login($_userForm);

        RestHttpResponse::json(array('isAdmin' => $userBean->getIsadmin()));
    }

    /**
     * 退出登录
     * @RequestMapping(value="", method="DELETE")
     * @throws RestException.
     */
    public function deleteSession() {
        if (!$this->_userSession->isLogin()) {
            throw new RestException(LangConst::USER_NOT_LOGIN,UserSessionErrorConstant::USER_NOT_LOGIN, RestHttpStatus::Unauthorized);
        }

        $this->_userSession->delete();

        RestHttpResponse::json(array());
    }
}