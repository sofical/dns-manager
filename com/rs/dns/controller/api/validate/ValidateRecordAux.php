<?php
namespace com\rs\dns\controller\api\validate;

use com\rs\dns\constant\RecordConstant;
use com\rs\dns\controller\api\vo\RecordForm;
use com\rs\dns\exception\BaException;
use restphp\exception\RestException;
use restphp\utils\RestStringUtils;

/**
 * Class ValidateRecordAux
 * @package com\rs\dns\controller\api\validate
 */
final class ValidateRecordAux {
    /**
     * 参数是否正确.
     * @param $value
     * @param $message
     * @param $propName
     * @param RecordForm $classInstance
     * @throws BaException
     * @throws RestException
     */
    public static function check($value, $message, $propName, $classInstance) {
        if (null == $value || !is_array($value) || empty($value)) {
            throw new BaException(RecordConstant::RECORD_BALANCE_AUX_NOT_NULL);
        }

        foreach ($value as $item) {
            if (!isset($item['id']) || !isset($item['aux']) || !is_numeric($item['id']) || $item['id'] < 1
                || !is_numeric($item['aux']) || $item['aux'] < 1) {
                throw new BaException(RecordConstant::RECORD_BALANCE_AUX_NOT_NULL);
            }
        }
    }
}