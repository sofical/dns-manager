<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\controller\BaseController;
use com\rs\dns\service\DomainService;
use com\rs\dns\service\RecordService;
use com\rs\dns\service\StatisticsService;

/**
 * Class UserStatisticsController.
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/user/statistics")
 */
final class UserStatisticsController extends BaseController {
    /**
     * 获取域名统计.
     * @RequestMapping(value="/domain", method="GET")
     */
    public function getDomain() {
        $info = DomainService::statisticsUser($this->_getUsername());
        $this->_success($info);
    }

    /**
     * 获取解析统计.
     * @RequestMapping(value="/record", method="GET")
     */
    public function getRecord() {
        $info = RecordService::statisticsUser($this->_getUsername());
        $this->_success($info);
    }
}