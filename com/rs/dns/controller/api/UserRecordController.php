<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\controller\api\vo\RecordForm;
use com\rs\dns\controller\api\vo\RecordQuery;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\filter\ApiFilter;
use com\rs\dns\service\DomainService;
use com\rs\dns\service\RecordService;
use restphp\http\RestHttpRequest;
use restphp\http\RestHttpStatus;
use restphp\utils\RestLog;


/**
 * Class UserRecordController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/user/records")
 */
final class UserRecordController extends BaseController {
    /**
     * 域名管理列表.
     * @RequestMapping(value="", method="GET")
     */
    public function recordList() {
        $pageParam = RestHttpRequest::getPageParam();
        $recordQuery = RestHttpRequest::getParameterAsObject(new RecordQuery());
        $domainList = RecordService::getRecordList($recordQuery, $pageParam, $this->_getUsername());
        $this->_success($domainList);
    }

    /**
     * 新增域名.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     * @throws BaException
     */
    public function add() {
        $recordForm = RestHttpRequest::getRequestBody(new RecordForm());
        if (!DomainService::hadDomain($recordForm->getOrigin(), $this->_getUsername())) {
            ApiFilter::autoCreateDomain($this->_getUsername(), $recordForm->getOrigin());
        }
        if (!DomainService::hadDomain($recordForm->getOrigin(), $this->_getUsername())) {
            throw new BaException(CommonConstant::NO_RIGHT_TO_OPERATING_THE_DATA, RestHttpStatus::Forbidden);
        }
        RestHttpRequest::getRequestBody(new RecordForm(), true);
        RecordService::add($recordForm);
    }

    /**
     * 修改域名信息.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws BaException
     */
    public function modify() {
        $recordForm = RestHttpRequest::getRequestBody(new RecordForm(), true);
        $id = RestHttpRequest::getPathValue("id");
        if (!RecordService::hadRecord($id, $this->_getUsername())) {
            throw new BaException(CommonConstant::NO_RIGHT_TO_OPERATING_THE_DATA, RestHttpStatus::Forbidden);
        }
        RecordService::modify($id, $recordForm);
    }

    /**
     * 单个删除域名.
     * @RequestMapping(value="/{id}", method="DELETE")
     * @throws BaException
     */
    public function delete() {
        $id = RestHttpRequest::getPathValue("id");
        if (!RecordService::hadRecord($id, $this->_getUsername())) {
            throw new BaException(CommonConstant::NO_RIGHT_TO_OPERATING_THE_DATA, RestHttpStatus::Forbidden);
        }
        RecordService::delete($id);
    }

    /**
     * 批量删除域名.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteDomainBatch() {
        $arrId = RestHttpRequest::getRequestBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(CommonConstant::DATA_TO_DELETE_CAN_NOT_NULL);
        }
        foreach ($arrId as $val) {
            if (!is_integer($val)) {
                throw new BaException(CommonConstant::PARAM_ERROR);
            }
        }
        if (!RecordService::hadRecords($arrId, $this->_getUsername())) {
            throw new BaException(CommonConstant::NO_RIGHT_TO_OPERATING_THE_DATA, RestHttpStatus::Forbidden);
        }
        RecordService::batchDelete($arrId);
    }
}