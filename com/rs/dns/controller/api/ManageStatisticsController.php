<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\controller\BaseController;
use com\rs\dns\service\DomainService;
use com\rs\dns\service\RecordService;
use com\rs\dns\service\StatisticsService;

/**
 * Class ManageStatisticsController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/manage/statistics")
 */
final class ManageStatisticsController extends BaseController {
    /**
     * 获取会员统计.
     * @RequestMapping(value="/member", method="GET")
     */
    public function getMember() {
        $info = StatisticsService::member();
        $this->_success($info);
    }

    /**
     * 会员最近7天注册趋势.
     * @RequestMapping(value="/member/trend", method="GET")
     */
    public function getMemberTrend() {
        $info = StatisticsService::memberTrend();
        $this->_success($info);
    }

    /**
     * 获取域名统计.
     * @RequestMapping(value="/domain", method="GET")
     */
    public function getDomain() {
        $info = DomainService::statisticsAll();
        $this->_success($info);
    }

    /**
     * 获取解析统计.
     * @RequestMapping(value="/record", method="GET")
     */
    public function getRecord() {
        $info = RecordService::statisticsType();
        $this->_success($info);
    }
}