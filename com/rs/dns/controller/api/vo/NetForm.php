<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class NetForm
 * @package com\rs\dns\controller\api\vo
 */
final class NetForm {
    /**
     * @var string 名称.
     * @length(min=1,max=50,message=名称长度不正确)
     */
    private $_netname;

    /**
     * @var string 描述.
     * @length(min=1,max=50,message=描述长度不正确)
     */
    private $_remark;

    /**
     * @var int 优先级.
     * @int(min=0,message=必须为0或正整数)
     */
    private $_aux;

    /**
     * @return string
     */
    public function getNetname()
    {
        return $this->_netname;
    }

    /**
     * @param string $netname
     */
    public function setNetname($netname)
    {
        $this->_netname = $netname;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->_remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->_remark = $remark;
    }

    /**
     * @return int
     */
    public function getAux()
    {
        return $this->_aux;
    }

    /**
     * @param int $aux
     */
    public function setAux($aux)
    {
        $this->_aux = $aux;
    }
}