<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class NetItemInfo
 * @package com\rs\dns\controller\api\vo
 */
final class NetItemInfo {
    /**
     * @var integer 行号.
     */
    private $_RowNumber;

    /**
     * @var integer 网络组ID.
     */
    private $_id;

    /**
     * @var string 网络名称.
     */
    private $_netname;

    /**
     * @var string 描述.
     */
    private $_remark;

    /**
     * @var integer 优先级.
     */
    private $_aux;

    /**
     * @return int
     */
    public function getRowNumber()
    {
        return $this->_RowNumber;
    }

    /**
     * @param int $RowNumber
     */
    public function setRowNumber($RowNumber)
    {
        $this->_RowNumber = $RowNumber;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getNetname()
    {
        return $this->_netname;
    }

    /**
     * @param string $netname
     */
    public function setNetname($netname)
    {
        $this->_netname = $netname;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->_remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->_remark = $remark;
    }

    /**
     * @return int
     */
    public function getAux()
    {
        return $this->_aux;
    }

    /**
     * @param int $aux
     */
    public function setAux($aux)
    {
        $this->_aux = $aux;
    }
}