<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class CommonQuery
 * @package com\rs\dns\controller\api\vo
 */
final class CommonQuery {
    /**
     * @var string 查询关键字.
     */
    private $_keyword;

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->_keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword($keyword)
    {
        $this->_keyword = $keyword;
    }
}