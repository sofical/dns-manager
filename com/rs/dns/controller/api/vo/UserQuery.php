<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class UserQuery
 * @package com\controller\api\vo
 */
final class UserQuery {
    /**
     * @var string 账号查询条件.
     */
    private $_account;

    /**
     * @var string 手机查询条件.
     */
    private $_mobile;

    /**
     * @var string 真实姓名查询条件.
     */
    private $_realName;

    /**
     * @var string 备注查询条件.
     */
    private $_verify;

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->_account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->_account = $account;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->_mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->_mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getRealName()
    {
        return $this->_realName;
    }

    /**
     * @param string $realName
     */
    public function setRealName($realName)
    {
        $this->_realName = $realName;
    }

    /**
     * @return string
     */
    public function getVerify()
    {
        return $this->_verify;
    }

    /**
     * @param string $remark
     */
    public function setVerify($remark)
    {
        $this->_verify = $remark;
    }


}