<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class SnForm
 * @package com\rs\dns\constant\api\vo
 */
final class SnForm {
    /**
     * @var string
     * @notnull(message=会员名不能为空)
     */
    private $_username;
    /**
     * @var string
     * @notnull(message=软件不能为空)
     */
    private $_soft;
    /**
     * @var string
     * @notnull(message=请输入序列号)
     */
    private $_sn;
    /**
     * @var string
     * @notnull(message=请输入状态)
     */
    private $_status;
    /**
     * @var string
     */
    private $_regnum = '';

    /**
     * @var string
     * @notnull(message=请输入机器码更换已使用次数)
     */
    private $_usenum;
    /**
     * @var string
     * @notnull(message=请输入最大机器码更换次数)
     */
    private $_allnum;
    /**
     * @var string
     * @notnull(message=请输入授权开始时间)
     */
    private $_bDate;
    /**
     * @var string
     * @notnull(message=请输入授权结束时间)
     */
    private $_eDate;
    /**
     * @var string
     */
    private $_remark2 = '';

    /**
     * =============================
     */

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return string
     */
    public function getSoft()
    {
        return $this->_soft;
    }

    /**
     * @param string $soft
     */
    public function setSoft($soft)
    {
        $this->_soft = $soft;
    }

    /**
     * @return string
     */
    public function getSn()
    {
        return $this->_sn;
    }

    /**
     * @param string $sn
     */
    public function setSn($sn)
    {
        $this->_sn = $sn;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * @return string
     */
    public function getRegnum()
    {
        return $this->_regnum;
    }

    /**
     * @param string $regnum
     */
    public function setRegnum($regnum)
    {
        $this->_regnum = $regnum;
    }

    /**
     * @return string
     */
    public function getUsenum()
    {
        return $this->_usenum;
    }

    /**
     * @param string $usenum
     */
    public function setUsenum($usenum)
    {
        $this->_usenum = $usenum;
    }

    /**
     * @return string
     */
    public function getAllnum()
    {
        return $this->_allnum;
    }

    /**
     * @param string $allnum
     */
    public function setAllnum($allnum)
    {
        $this->_allnum = $allnum;
    }

    /**
     * @return string
     */
    public function getBDate()
    {
        return $this->_bDate;
    }

    /**
     * @param string $bDate
     */
    public function setBDate($bDate)
    {
        $this->_bDate = $bDate;
    }

    /**
     * @return string
     */
    public function getEDate()
    {
        return $this->_eDate;
    }

    /**
     * @param string $eDate
     */
    public function setEDate($eDate)
    {
        $this->_eDate = $eDate;
    }

    /**
     * @return string
     */
    public function getRemark2()
    {
        return $this->_remark2;
    }

    /**
     * @param string $remark2
     */
    public function setRemark2($remark2)
    {
        $this->_remark2 = $remark2;
    }


    /**
     * ========================
     */


}