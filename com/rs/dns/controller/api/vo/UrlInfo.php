<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class UrlInfo
 * @package com\rs\dns\controller\api\vo
 */
final class UrlInfo {
    /**
     * @var integer id.
     */
    private $_ID;

    /**
     * @var string 所属会员账号.
     */
    private $_username;

    /**
     * @var string 记录完整名称.
     */
    private $_domain;

    /**
     * @var string 所属域名.
     */
    private $_origin;

    /**
     * @var integer 所属域名ID.
     */
    private $_zone;

    /**
     * @var string 主机头名称.
     */
    private $_name;

    /**
     * @var integer 网络组（解析线路）ID.
     */
    private $_netid;

    /**
     * @var string 网络组（解析线路）名称.
     */
    private $_netname;

    /**
     * @var string 转发地址.
     */
    private $_URL;

    /**
     * @var integer 转发类型：0-显性转发，1-隐性转发.
     */
    private $_URLtype;

    /**
     * @var string 标题.
     */
    private $_title;

    /**
     * @return int
     */
    public function getID()
    {
        return $this->_ID;
    }

    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->_ID = $ID;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->_domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->_domain = $domain;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->_origin = $origin;
    }

    /**
     * @return int
     */
    public function getZone()
    {
        return $this->_zone;
    }

    /**
     * @param int $zone
     */
    public function setZone($zone)
    {
        $this->_zone = $zone;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return int
     */
    public function getNetid()
    {
        return $this->_netid;
    }

    /**
     * @param int $netid
     */
    public function setNetid($netid)
    {
        $this->_netid = $netid;
    }

    /**
     * @return string
     */
    public function getNetname()
    {
        return $this->_netname;
    }

    /**
     * @param string $netname
     */
    public function setNetname($netname)
    {
        $this->_netname = $netname;
    }

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->_URL;
    }

    /**
     * @param string $URL
     */
    public function setURL($URL)
    {
        $this->_URL = $URL;
    }

    /**
     * @return int
     */
    public function getURLtype()
    {
        return $this->_URLtype;
    }

    /**
     * @param int $URLtype
     */
    public function setURLtype($URLtype)
    {
        $this->_URLtype = $URLtype;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }


}