<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class DomainInfo
 * @package com\rs\dns\controller\api\vo
 */
final class DomainInfo {
    /**
     * @var string 主键ID.
     */
    private $_id;

    /**
     * @var string 域名.
     */
    private $_origin;

    /**
     * @var string 状态.
     */
    private $_yn;

    /**
     * @var string 开始时间.
     */
    private $_topdate;

    /**
     * @var string 结束时间.
     */
    private $_enddate;

    /**
     * @var string 所属会员账号.
     */
    private $_username;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->_origin = $origin;
    }

    /**
     * @return string
     */
    public function getYn()
    {
        return $this->_yn;
    }

    /**
     * @param string $yn
     */
    public function setYn($yn)
    {
        $this->_yn = $yn;
    }

    /**
     * @return string
     */
    public function getTopdate()
    {
        return $this->_topdate;
    }

    /**
     * @param string $topdate
     */
    public function setTopdate($topdate)
    {
        $this->_topdate = $topdate;
    }

    /**
     * @return string
     */
    public function getEnddate()
    {
        return $this->_enddate;
    }

    /**
     * @param string $enddate
     */
    public function setEnddate($enddate)
    {
        $this->_enddate = $enddate;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }
}