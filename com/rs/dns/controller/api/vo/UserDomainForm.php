<?php
namespace com\rs\dns\controller\api\vo;

/**
 * 域名表单.
 * Class UserDomainForm
 * @package com\rs\dns\api\vo
 */
final class UserDomainForm {
    /**
     * @var string 域名.
     * @customer(method='\restphp\validate\RestValidateDomain::customer',message=域名格式不正确)
     */
    private $_origin;

    /**
     * @var string 用户.
     */
    private $_username;

    /**
     * @var string 域名状态.
     * @inArray(value=[0|1],message=域名状态不在可选范围内)
     */
    private $_yn;

    /**
     * @var string 开始时间.
     * @date(format='Y-m-d H:i:s')
     */
    private $_topdate;

    /**
     * @var string 结束时间.
     * @date(format='Y-m-d H:i:s')
     */
    private $_enddate;

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->_origin = $origin;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return string
     */
    public function getYn()
    {
        return $this->_yn;
    }

    /**
     * @param string $yn
     */
    public function setYn($yn)
    {
        $this->_yn = $yn;
    }

    /**
     * @return string
     */
    public function getTopdate()
    {
        return $this->_topdate;
    }

    /**
     * @param string $topdate
     */
    public function setTopdate($topdate)
    {
        $this->_topdate = $topdate;
    }

    /**
     * @return string
     */
    public function getEnddate()
    {
        return $this->_enddate;
    }

    /**
     * @param string $enddate
     */
    public function setEnddate($enddate)
    {
        $this->_enddate = $enddate;
    }
}