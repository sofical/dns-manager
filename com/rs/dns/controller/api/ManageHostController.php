<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\constant\HostConstant;
use com\rs\dns\controller\api\vo\CommonQuery;
use com\rs\dns\controller\api\vo\HostBatchForm;
use com\rs\dns\controller\api\vo\HostForm;
use com\rs\dns\controller\api\vo\Ipv6BatchForm;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\HostService;
use com\rs\dns\service\Ipv6Service;
use restphp\http\RestHttpRequest;

/**
 * Class ManageHostController
 * @package com\rs\dns\controller
 * @RequestMapping("/api/manage/hosts")
 */
final class ManageHostController extends BaseController {
    /**
     * 获取主机列表.
     * @RequestMapping(value="", method="GET")
     */
    public function getHostList() {
        $pageParam = RestHttpRequest::getPageParam();
        $commonQuery = RestHttpRequest::getParameterAsObject(new CommonQuery());
        $ipList = HostService::getHostList($commonQuery, $pageParam);
        $this->_success($ipList);
    }

    /**
     * 添加IPV6地址分配.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     */
    public function add() {
        $hostForm = RestHttpRequest::getBody(new HostForm(), true);
        HostService::add($hostForm);
    }

    /**
     * 修改IPV6地址分配.
     * @RequestMapping(value="/{ip}", method="PUT")
     * @throws \ReflectionException
     * @throws BaException
     */
    public function modify() {
        $ip = RestHttpRequest::getPathValue("ip");
        $hostForm = RestHttpRequest::getBody(new HostForm(), true);
        if ($ip != $hostForm->getIP()) {
            throw new BaException(HostConstant::HOST_IP_NOT_SAME);
        }
        HostService::modify($ip, $hostForm);
    }

    /**
     * 删除IPV6地址.
     * @RequestMapping(value="/{ip}", method="DELETE")
     */
    public function delete() {
        $ip = RestHttpRequest::getPathValue("ip");
        HostService::delete($ip);
    }

    /**
     * 批量删除.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteBatch() {
        $arrId = RestHttpRequest::getBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(CommonConstant::DATA_TO_DELETE_CAN_NOT_NULL);
        }
        HostService::deleteBatch($arrId);
    }

    /**
     * 批量新增.
     * @RequestMapping(value="/actions/batch_add", method="POST")
     * @throws \ReflectionException
     */
    public function batchAdd() {
        $hostBatchForm = RestHttpRequest::getBody(new HostBatchForm(), true);
        HostService::batchAdd($hostBatchForm);
    }
}