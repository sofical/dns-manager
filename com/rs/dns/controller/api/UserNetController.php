<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\controller\api\vo\CommonQuery;
use com\rs\dns\controller\api\vo\NetForm;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\NetService;
use restphp\http\RestHttpRequest;

/**
 * Class UserNetController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/user/nets")
 */
final class UserNetController extends BaseController {
    /**
     * 获取所有可用网络.
     * @RequestMapping(value="/action/collect", method="GET")
     */
    public function getCollect() {
        $collect = NetService::getCollect();
        $this->_success($collect);
    }
}