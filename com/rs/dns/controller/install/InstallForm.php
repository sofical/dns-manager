<?php
namespace com\rs\dns\controller\install;

/**
 * Class InstallForm
 * @package com\rs\dns\controller\install
 */
final class InstallForm {
    /**
     * @var string 数据库服务地址.
     * @notnull(message="数据库服务地址不能为空")
     */
    private $_dbServer;

    /**
     * @var string 数据库端口.
     * @notnull(message="数据库端口不能为空")
     */
    private $_dbPort;

    /**
     * @var string 数据库用户名.
     * @notnull(message="数据库用户名不能为空")
     */
    private $_dbUsername;

    /**
     * @var string 数据库登录密码.
     * @notnull(message="数据库登录密码不能为空")
     */
    private $_dbPassword;

    /**
     * @var string 数据库密码.
     * @notnull(message="数据库密码不能为空")
     */
    private $_dbName;

    /**
     * @var string 网站管理账号.
     * @length(min=4,max=16,message="请输入网站管理平台管理账号，长度为4-16字符")
     */
    private $_manageUsername;

    /**
     * @var string 网站管理密码.
     * @length(min=6,max=16,message="请输入网站管理平台管理账号登录密码, 长度为6-16字符")
     */
    private $_managePassword;

    /**
     * @return string
     */
    public function getDbServer()
    {
        return $this->_dbServer;
    }

    /**
     * @param string $dbServer
     */
    public function setDbServer($dbServer)
    {
        $this->_dbServer = $dbServer;
    }

    /**
     * @return string
     */
    public function getDbPort()
    {
        return $this->_dbPort;
    }

    /**
     * @param string $dbPort
     */
    public function setDbPort($dbPort)
    {
        $this->_dbPort = $dbPort;
    }

    /**
     * @return string
     */
    public function getDbUsername()
    {
        return $this->_dbUsername;
    }

    /**
     * @param string $dbUsername
     */
    public function setDbUsername($dbUsername)
    {
        $this->_dbUsername = $dbUsername;
    }

    /**
     * @return string
     */
    public function getDbPassword()
    {
        return $this->_dbPassword;
    }

    /**
     * @param string $dbPassword
     */
    public function setDbPassword($dbPassword)
    {
        $this->_dbPassword = $dbPassword;
    }

    /**
     * @return string
     */
    public function getDbName()
    {
        return $this->_dbName;
    }

    /**
     * @param string $dbName
     */
    public function setDbName($dbName)
    {
        $this->_dbName = $dbName;
    }

    /**
     * @return string
     */
    public function getManageUsername()
    {
        return $this->_manageUsername;
    }

    /**
     * @param string $manageUsername
     */
    public function setManageUsername($manageUsername)
    {
        $this->_manageUsername = $manageUsername;
    }

    /**
     * @return string
     */
    public function getManagePassword()
    {
        return $this->_managePassword;
    }

    /**
     * @param string $managePassword
     */
    public function setManagePassword($managePassword)
    {
        $this->_managePassword = $managePassword;
    }
}