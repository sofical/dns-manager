<?php
namespace com\rs\dns\service\user\impl;

use com\rs\dns\repository\UserLogRepository;
use com\rs\dns\service\user\UserLogService;
use restphp\http\request\RestHttpRequestIP;
use restphp\utils\RestUUIDUtil;

/**
 * Class UserLogServiceImpl
 * @package php\service\user\impl
 */
class UserLogServiceImpl implements UserLogService {
    /**
     * 添加日志.
     * @param $nLogType
     * @param $strRemark
     * @return mixed|void
     */
    public function log($nLogType, $strRemark) {
        $userSessionService = new UserSessionServiceImpl();
        $userSession = $userSessionService->getSession();
        $userLogRepository = new UserLogRepository();
        $userLogRepository->insert(array(
            'logId' => RestUUIDUtil::guid(''),
            'userId' => null == $userSession ? 0 : $userSession->getUserId(),
            'logType' => $nLogType,
            'remark' => $strRemark,
            'ip' => RestHttpRequestIP::getAgentIp()
        ));
    }

}