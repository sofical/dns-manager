<?php
namespace com\rs\dns\service;

use com\rs\dns\constant\NetConstant;
use com\rs\dns\controller\api\vo\CommonQuery;
use com\rs\dns\controller\api\vo\NetForm;
use com\rs\dns\controller\api\vo\NetInfo;
use com\rs\dns\controller\api\vo\NetItemInfo;
use com\rs\dns\exception\BaException;
use com\rs\dns\repository\NetRepository;
use restphp\biz\PageParam;
use restphp\utils\RestClassUtils;
use restphp\utils\RestStringUtils;

/**
 * Class NetService
 * @package com\rs\dns\service
 */
final class NetService {
    /**
     * 获取线路管理列表.
     * @param CommonQuery $commonQuery
     * @param PageParam $pageParam
     * @return \restphp\biz\RestPageReturn
     */
    public static function getNetList($commonQuery, $pageParam) {
        $arrRule = array(
        );
        if (!RestStringUtils::isBlank($commonQuery->getKeyword())) {
            $keyword = '%' . $commonQuery->getKeyword() . '%';
            $arrRule['k1'] = array(
                ' netname like ? or remark like ? ',
                array($keyword, $keyword, $keyword)
            );
        }

        $arrParams = array(
            'output' => ' * ',
            'rule' => $arrRule,
            'page_param' => $pageParam,
            'order' => ' order by netname '
        );
        $netRepository = new NetRepository();
        return $netRepository->getPageReturn($arrParams, new NetItemInfo());
    }

    /**
     * 添加线路.
     * @param NetForm $netForm
     * @throws BaException
     * @throws \restphp\exception\RestException
     * @throws \ReflectionException
     */
    public static function add($netForm) {
        //检查是否有相同名称的线路存在
        $arrExistRule = array(
            'netname' => $netForm->getNetname()
        );
        $netRepository = new NetRepository();
        $exist = $netRepository->findOne($arrExistRule);
        if (null != $exist) {
            throw new BaException(NetConstant::NET_NAME_EXIST);
        }

        $netRepository->save($netForm);
    }

    /**
     * 修改线路.
     * @param int $id
     * @param NetForm $netForm
     * @throws BaException
     */
    public static function modify($id, $netForm) {
        //检查是否有相同名称的线路存在
        $arrExistRule = array(
            'netname' => $netForm->getNetname(),
            'id' => array(
                'id<>?',
                $id
            )
        );
        $netRepository = new NetRepository();
        $exist = $netRepository->fineOne($arrExistRule);
        if (null != $exist) {
            throw new BaException(NetConstant::NET_NAME_EXIST);
        }

        $arrRule = array(
            'id' => $id
        );

        $arrUpdate = RestClassUtils::beanToArr($netForm);

        $netRepository->update($arrUpdate, $arrRule);
    }

    /**
     * 批量删除.
     * @param $arrId
     */
    public static function deleteBatch($arrId) {
        $strDelRule = '';
        $arrSqlParam = array();
        foreach ($arrId as $id) {
            $strDelRule .= ('' == $strDelRule ? '' : ',') . '?';
            $arrSqlParam[] = $id;
        }
        $arrRule = array(
            "k" => array(
                " id in ({$strDelRule}) ",
                $arrSqlParam
            )
        );
        $netRepository = new NetRepository();
        $netRepository->delete($arrRule);
    }

    /**
     * get select collect.
     * @return array
     */
    public static function getCollect() {
        $arrParams = array(
            'output' => ' id, netname ',
            'order' => ' order by netname '
        );
        $netRepository = new NetRepository();
        return $netRepository->select($arrParams, new NetInfo());
    }

    /**
     * 通过ID获取网络组信息.
     * @param int $id
     * @return mixed|null
     */
    public static function getOne($id) {
        $arrRule = array(
            'id' => $id
        );
        $netRepository = new NetRepository();
        return $netRepository->fineOne($arrRule);
    }
}