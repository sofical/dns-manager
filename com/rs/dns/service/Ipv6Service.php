<?php
namespace com\rs\dns\service;

use com\rs\dns\controller\api\vo\IPQuery;
use com\rs\dns\controller\api\vo\Ipv4Form;
use com\rs\dns\controller\api\vo\Ipv6BatchForm;
use com\rs\dns\controller\api\vo\Ipv6Form;
use com\rs\dns\controller\api\vo\Ipv6Info;
use com\rs\dns\repository\IpTableV6Repository;
use restphp\biz\PageParam;
use restphp\utils\RestClassUtils;
use restphp\utils\RestStringUtils;

/**
 * Class Ipv6Service
 * @package com\rs\dns\service
 */
final class Ipv6Service {
    /**
     * 获取IP分配列表.
     * @param IPQuery $ipQuery
     * @param PageParam $pageParam
     * @return \restphp\biz\RestPageReturn
     */
    public static function getIpList($ipQuery, $pageParam) {
        $arrRule = array(
        );
        if (!RestStringUtils::isBlank($ipQuery->getKeyword())) {
            $keyword = '%' . $ipQuery->getKeyword() . '%';
            $arrRule['k1'] = array(
                ' ip like ? or remark like ? ',
                array($keyword, $keyword)
            );
        }
        if (!RestStringUtils::isBlank($ipQuery->getNetId())) {
            $arrRule['netid'] = $ipQuery->getNetId();
        }

        $arrParams = array(
            'output' => ' * ',
            'rule' => $arrRule,
            'page_param' => $pageParam,
            'order' => ' order by ip '
        );
        $ipTableRepository = new IpTableV6Repository();
        return $ipTableRepository->getPageReturn($arrParams, new Ipv6Info());
    }

    /**
     * 添加IPV6分配地址.
     * @param Ipv6Form $ipv6Form
     * @throws \ReflectionException
     */
    public static function add($ipv6Form) {
        // todo: 检查重复地址.
        $ipTableRepository = new IpTableV6Repository();
        $ipTableRepository->save($ipv6Form);
    }

    /**
     * 修改IPV6分配地址.
     * @param $id
     * @param Ipv6Form $ipv6Form
     * @throws \ReflectionException
     */
    public static function modify($id, $ipv6Form) {
        // todo: 检查重复地址.
        $arrRule = array(
            'id' => $id
        );
        $arrData = RestClassUtils::beanToArr($ipv6Form);
        $arrData['isChange'] = 1;
        $ipTableRepository = new IpTableV6Repository();
        $ipTableRepository->update($arrData, $arrRule);
    }

    /**
     * 删除IPV6配置地址.
     * @param $id
     */
    public static function delete($id) {
        $arrRule = array(
            'id' => $id
        );
        $ipTableRepository = new IpTableV6Repository();
        $ipTableRepository->delete($arrRule);
    }

    /**
     * 批昌删除.
     * @param $arrId
     */
    public static function deleteBatch($arrId) {
        $strDelRule = '';
        $arrSqlParam = array();
        foreach ($arrId as $id) {
            $strDelRule .= ('' == $strDelRule ? '' : ',') . '?';
            $arrSqlParam[] = $id;
        }
        $arrRule = array(
            "k" => array(
                " id in ({$strDelRule}) ",
                $arrSqlParam
            )
        );
        $ipTableRepository = new IpTableV6Repository();
        $ipTableRepository->delete($arrRule);
    }

    /**
     * 批量添加
     * @param Ipv6BatchForm $ipv6BatchForm
     * @throws \ReflectionException
     */
    public static function batchAdd($ipv6BatchForm) {
        // todo: 判断重复.
        $ipTableRepository = new IpTableV6Repository();
        $arrLine = explode("\n", $ipv6BatchForm->getIpTxt());
        foreach ($arrLine as $line) {
            //先删除相同IP.
            $arrCol = explode('/', $line);
            $arrDeleteRule = array(
                'ip' => $arrCol[0],
            );
            $ipTableRepository->delete($arrDeleteRule);
            //新增数.
            $ipv6Form = new Ipv6Form();
            $ipv6Form->setIp($arrCol[0]);
            $ipv6Form->setMask($arrCol[1]);
            $ipv6Form->setNetid($ipv6BatchForm->getNetid());
            $ipTableRepository->save($ipv6Form);
        }
    }
}