<?php
namespace com\rs\dns\service;

use com\rs\dns\controller\api\vo\IPQuery;
use com\rs\dns\controller\api\vo\Ipv4BatchForm;
use com\rs\dns\controller\api\vo\Ipv4Form;
use com\rs\dns\controller\api\vo\Ipv4Info;
use com\rs\dns\repository\IpTableRepository;
use restphp\biz\PageParam;
use restphp\utils\RestClassUtils;
use restphp\utils\RestStringUtils;

/**
 * Class Ipv4Service
 * @package com\rs\dns\service
 */
final class Ipv4Service {
    /**
     * 获取IP分配列表.
     * @param IPQuery $ipQuery
     * @param PageParam $pageParam
     * @return \restphp\biz\RestPageReturn
     */
    public static function getIpList($ipQuery, $pageParam) {
        $arrRule = array(
        );
        if (!RestStringUtils::isBlank($ipQuery->getKeyword())) {
            $keyword = '%' . $ipQuery->getKeyword() . '%';
            $arrRule['k1'] = array(
                ' ip like ? or remark like ? ',
                array($keyword, $keyword)
            );
        }
        if (!RestStringUtils::isBlank($ipQuery->getNetId())) {
            $arrRule['netid'] = $ipQuery->getNetId();
        }

        $arrParams = array(
            'output' => ' * ',
            'rule' => $arrRule,
            'page_param' => $pageParam,
            'order' => ' order by ip '
        );
        $ipTableRepository = new IpTableRepository();
        return $ipTableRepository->getPageReturn($arrParams, new Ipv4Info());
    }

    /**
     * 添加IPV4分配地址.
     * @param Ipv4Form $ipv4Form
     * @throws \ReflectionException
     */
    public static function add($ipv4Form) {
        // todo: 检查重复地址.
        $ipTableRepository = new IpTableRepository();
        $ipTableRepository->save($ipv4Form);
    }

    /**
     * 修改IPV4分配地址.
     * @param $id
     * @param Ipv4Form $ipv4Form
     * @throws \ReflectionException
     */
    public static function modify($id, $ipv4Form) {
        // todo: 检查重复地址.
        $arrRule = array(
            'id' => $id
        );
        $arrData = RestClassUtils::beanToArr($ipv4Form);
        $ipTableRepository = new IpTableRepository();
        $ipTableRepository->update($arrData, $arrRule);
    }

    /**
     * 删除IPV4配置地址.
     * @param $id
     */
    public static function delete($id) {
        $arrRule = array(
            'id' => $id
        );
        $ipTableRepository = new IpTableRepository();
        $ipTableRepository->delete($arrRule);
    }

    /**
     * 批昌删除.
     * @param $arrId
     */
    public static function deleteBatch($arrId) {
        $strDelRule = '';
        $arrSqlParam = array();
        foreach ($arrId as $id) {
            $strDelRule .= ('' == $strDelRule ? '' : ',') . '?';
            $arrSqlParam[] = $id;
        }
        $arrRule = array(
            "k" => array(
                " id in ({$strDelRule}) ",
                $arrSqlParam
            )
        );
        $ipTableRepository = new IpTableRepository();
        $ipTableRepository->delete($arrRule);
    }

    /**
     * 批量添加
     * @param Ipv4BatchForm $ipv4BatchForm
     * @throws \ReflectionException
     */
    public static function batchAdd($ipv4BatchForm) {
        // todo: 判断重复.
        $ipTableRepository = new IpTableRepository();
        $arrLine = explode("\n", $ipv4BatchForm->getIpTxt());
        foreach ($arrLine as $line) {
            //先删除相同IP.
            $arrCol = explode(' ', $line);
            $arrDeleteRule = array(
                'ip' => $arrCol[0],
            );
            $ipTableRepository->delete($arrDeleteRule);
            //新增数.
            $ipv4Form = new Ipv4Form();
            $ipv4Form->setIp($arrCol[0]);
            $ipv4Form->setMask($arrCol[1]);
            $ipv4Form->setNetid($ipv4BatchForm->getNetid());
            $ipTableRepository->save($ipv4Form);
        }
    }
}