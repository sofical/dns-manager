<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class SoaRepository
 * @package com\rs\dns\repository
 */
final class SoaRepository extends RestMSRepository {
    public function __construct() {
        parent::__construct('soa');
    }
}