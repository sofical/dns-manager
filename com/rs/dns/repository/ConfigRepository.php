<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class ConfigRepository
 * @package com\rs\dns\repository
 */
final class ConfigRepository extends RestMSRepository {
    public function __construct(){
        parent::__construct('config');
    }
}