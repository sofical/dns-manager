<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class IpTableRepository
 * @package com\rs\dns\repository
 */
final class IpTableV6Repository extends RestMSRepository {
    public function __construct()
    {
        parent::__construct('iptablev6');
    }
}