<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class RrRepository
 * @package com\rs\dns\repository
 */
final class RrRepository extends RestMSRepository {
    public function __construct() {
        parent::__construct('rr');
    }
}