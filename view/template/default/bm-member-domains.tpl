<div id="member-domains">
    <div style="padding: 20px; margin-top: -50px">
        <div style="padding: 10px">
            <el-breadcrumb separator-class="el-icon-arrow-right">
                <el-breadcrumb-item>域名管理</el-breadcrumb-item>
            </el-breadcrumb>
        </div>
        <div style="border: 1px solid #f7f7f7; padding: 30px">
            <el-row>
                <el-form :inline="true" :model="formQuery" class="demo-form-inline">
                    <el-form-item label="域名：">
                        <el-input v-model="formQuery.domain" size="small" style="width: 160px" placeholder="域名" clearable></el-input>
                    </el-form-item>
                    <el-form-item label="状态：">
                        <el-select v-model="formQuery.yn" placeholder="请选择状态" size="small" clearable>
                            <el-option label="暂停" value="0"></el-option>
                            <el-option label="正常" value="1"></el-option>
                        </el-select>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" size="small" @click="onQuery">查询</el-button>
                    </el-form-item>
                </el-form>
            </el-row>
            <el-row>
                <el-button-group>
                    <el-button type="danger" size="small" icon="el-icon-s-check" style="float: left" @click="deleteAll">批量删除</el-button>
                    <el-button type="success" size="small" icon="el-icon-delete" style="float: left" @click="showMark">批量修改状态</el-button>
                </el-button-group>
                <el-button type="primary" icon="el-icon-circle-plus-outline" style="float: right" size="small" @click="showAdd">新增</el-button>
            </el-row>
            <el-row style="margin-top: 10px">
                <template>
                    <el-table
                            :data="tableData"
                            style="width: 100%"
                            border
                            stripe
                            v-loading="listLoading"
                            size="small"
                            @selection-change="handleSelectionChange">
                        <el-table-column
                                type="selection"
                                width="55">
                        </el-table-column>
                        <el-table-column
                                prop="id"
                                width="80"
                                label="序号">
                        </el-table-column>
                        <el-table-column
                                prop="origin"
                                label="域名">
                            <template scope="scope">
                                <a :href="'?menu=2-2&domain=' + scope.row.origin.substring(0, scope.row.origin.length - 1)" v-html="scope.row.origin"></a>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="yn"
                                label="状态">
                            <template scope="scope">
                                <span v-if="scope.row.yn == 0" style="color: red">暂停</span>
                                <span v-if="scope.row.yn == 1" style="color: green">正常</span>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="topdate"
                                label="开始时间">
                        </el-table-column>
                        <el-table-column
                                prop="enddate"
                                label="结束时间">
                        </el-table-column>
                        <el-table-column
                                width="280"
                                label="操作">
                            <template scope="scope">
                                <el-button type="primary" size="small" icon="el-icon-document" @click="goToSee(scope.row)" plain>解析</el-button>
                                <el-button type="primary" size="small" icon="el-icon-edit" @click="showEdit(scope.row)" plain>修改</el-button>
                                <el-button type="danger" size="small" icon="el-icon-delete" @click="deleteMessage(scope.row)" plain>删除</el-button>
                            </template>
                        </el-table-column>
                    </el-table>
                </template>
                <div class="page-block" >
                    <el-pagination
                            @current-change="handleCurrentChange"
                            layout="total,prev, pager, next"
                            :total= page.total
                            :page-size = page.pageSize
                            class="page"
                    >
                    </el-pagination>
                </div>
            </el-row>
        </div>

        <el-dialog :title="isNew ? '新增域名' : '修改域名'" :visible.sync="dialogFormVisible">
            <el-form :model="form" ref="form" :rules="rules" label-width="260px" size="small">
                <el-form-item label="域名：" prop="origin">
                    <el-input placeholder="请输入域名" v-model="form.origin" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="开始时间：" prop="topdate">
                    <el-date-picker type="datetime" value-format="yyyy-MM-dd HH:mm:ss" placeholder="选择开始时间" v-model="form.topdate" size="small"></el-date-picker>
                </el-form-item>
                <el-form-item label="结束时间：" prop="enddate">
                    <el-date-picker type="datetime" value-format="yyyy-MM-dd HH:mm:ss" placeholder="选择开始时间" v-model="form.enddate" size="small"></el-date-picker>
                </el-form-item>
                <el-form-item label="状态：" prop="yn">
                    <el-select v-model="form.yn" placeholder="请选择状态" size="small">
                        <el-option label="暂停" :value="0"></el-option>
                        <el-option label="正常" :value="1"></el-option>
                    </el-select>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveEdit" size="small">确 定</el-button>
            </div>
        </el-dialog>

        <el-dialog title="批量修改状态" :visible.sync="dialogBatchStateFormVisible">
            <el-form :model="stateForm" ref="stateForm" :rules="stateRule" label-width="260px" size="small">
                <el-form-item label="状态：" prop="yn">
                    <el-select v-model="stateForm.yn" placeholder="请选择状态" size="small">
                        <el-option label="暂停" :value="0"></el-option>
                        <el-option label="正常" :value="1"></el-option>
                    </el-select>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogBatchStateFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveMark" size="small">确 定</el-button>
            </div>
        </el-dialog>

    </div>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script language="JavaScript" src="js/bm-member-domains.js"></script>