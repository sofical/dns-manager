/**
 * 管理后台中心首页.
 * @type {Vue}
 */
let vmManageRecords = new Vue({
    el: '#manage-records',
    data () {
        return {
            activeName: 'first'
        }
    },
    methods: {
        loadPage() {
            let tab = QiServletUtils.get("tab");
            let url = '/manage-records-dns';
            if (QiStringUtils.isBlank(tab)) {
                tab = 'first'
            }
            if ('first' !== tab) {
                url = '/manage-records-url';
            }
            this.activeName = tab;

            $('#manage-record-div').tpl(url, {}, function () {
            });
        },
        handleClick (tab, event) {
            let domain = QiServletUtils.get("domain");
            let username = QiServletUtils.get("username");
            domain = QiStringUtils.isBlank(domain) ? '' : decodeURI(domain);
            username = QiStringUtils.isBlank(username) ? '' : username;
            let url = '?menu=2-2&tab=' + tab.name + '&domain=' + domain + '&username=' + username;
            let iframeToken = QiServletUtils.get('iframe-token');
            if (!QiStringUtils.isBlank(iframeToken)) {
                url += "&iframe-token=" + iframeToken;
            }
            location.href = url;
        }
    },
    mounted () {
        this.loadPage();
    }
});