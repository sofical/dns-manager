/**
 * 管理后台中心首页.
 * @type {Vue}
 */
let vmMemberRecords = new Vue({
    el: '#member-records',
    data () {
        return {
            activeName: 'first'
        }
    },
    methods: {
        loadPage() {
            let tab = QiServletUtils.get("tab");
            let url = '/member-records-dns';
            if (QiStringUtils.isBlank(tab)) {
                tab = 'first'
            }
            if ('first' !== tab) {
                url = '/member-records-url';
            }
            this.activeName = tab;
            url += (iFrameToken ? ('?iframe-token=' + iFrameToken) : '');

            $('#manage-record-div').tpl(url, {}, function () {
            });
        },
        handleClick (tab, event) {
            let domain = QiServletUtils.get("domain");
            domain = QiStringUtils.isBlank(domain) ? '' : decodeURI(domain);
            let url = '?menu=2-2&tab=' + tab.name + '&domain=' + domain;
            let iframeToken = QiServletUtils.get('iframe-token');
            if (!QiStringUtils.isBlank(iframeToken)) {
                url += "&iframe-token=" + iframeToken;
            }
            location.href = url;
        }
    },
    mounted () {
        this.loadPage();
    }
});