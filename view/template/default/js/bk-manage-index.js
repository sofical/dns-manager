/**
 * 管理后台中心首页.
 * @type {Vue}
 */
let vmManageIndex = new Vue({
    el: '#manage-index',
    data: function () {
        return {
            data : {
                member: {
                    uncheck: 0,
                    today: 0,
                    refuse: 0,
                    forbidden: 0,
                    normal: 0
                },
                memberTrend: [],
                domain: {
                    normal: 0,
                    stop: 0
                },
                record: {
                    A: 0,
                    MX: 0,
                    CNAME: 0,
                    NS: 0,
                    PTR: 0,
                    TXT: 0,
                    AAAA: 0,
                    SRV: 0,
                    '显示转发': 0,
                    '隐性转发': 0,
                    total: 0
                }
            }
        }
    },
    methods: {
        /**
         * 架载统计数据.
         */
        loadData () {
            //会员统计
            QiRestClient.get('/api/manage/statistics/member' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), (data) => {
                this.data.member = data;
                //渲染会员统计
                this.drawMember();
            });

            //会员注册趋势
            QiRestClient.get('/api/manage/statistics/member/trend' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), (data) => {
                this.data.memberTrend = data;
                //渲染会员注册趋势.
                this.memberTrend();
            });

            //域名统计.
            QiRestClient.get('/api/manage/statistics/domain' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), (data) => {
                this.data.domain = data;
                //渲染域名统计
                this.drawDomain();
            });

            //解析统计.
            QiRestClient.get('/api/manage/statistics/record' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), (data) => {
                this.data.record = data;
                //渲染解析统计
                this.drawRecord();
            });
        },

        /**
         * 渲染会员统计.
         */
        drawMember() {
            let dom = document.getElementById("member-draw");
            let myChart = echarts.init(dom);
            let data = [
                {value: this.data.member.uncheck, name: '未审核'},
                {value: this.data.member.normal, name: '正常'},
                {value: this.data.member.refuse, name: '拒绝'},
                {value: this.data.member.forbidden, name: '禁用'}
            ];
            let option = {
                title: {
                    text: '会员统计',
                    subtext: '',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['未审核', '正常', '禁用', '拒绝']
                },
                series: [
                    {
                        name: '会员',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: data,
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
        },

        /**
         * 渲染会员注册趋势.
         */
        memberTrend() {
            let dataTitle = [];
            let dataValue = [];
            this.data.memberTrend.forEach((item) => {
                dataTitle.push(item.date);
                dataValue.push(item.num);
            })
            let dom = document.getElementById("member-trend");
            let myChart = echarts.init(dom);
            let option = {
                xAxis: {
                    type: 'category',
                    data: dataTitle
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: dataValue,
                    type: 'line'
                }]
            };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
        },

        /**
         * 渲染域名统计.
         */
        drawDomain() {
            let dom = document.getElementById("domain-draw");
            let myChart = echarts.init(dom);
            let data = [
                {value: this.data.domain.stop, name: '暂停'},
                {value: this.data.domain.normal, name: '正常'}
            ];
            let option = {
                title: {
                    text: '域名统计',
                    subtext: '',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['暂停', '正常']
                },
                series: [
                    {
                        name: '域名',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: data,
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
        },

        /**
         * 渲染解析统计.
         */
        drawRecord() {
            let dom = document.getElementById("record-draw");
            let myChart = echarts.init(dom);
            let dataTitle = [];
            let dataValue = [];
            $.each(this.data.record, (name, value) => {
                if ('total' === name) {
                    return;
                }
                dataValue.push({
                    value: value,
                    name: name
                });
                dataTitle.push(name);
            });

            let option = {
                title: {
                    text: '域名统计',
                    subtext: '',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: dataTitle
                },
                series: [
                    {
                        name: '域名',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: dataValue,
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
        }
    },
    mounted: function () {
        this.loadData();
    }
});