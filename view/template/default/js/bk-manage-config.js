/**
 * 管理后台中心首页.
 * @type {Vue}
 */
let vmManageConfig = new Vue({
    el: '#manage-config',
    data: function () {
        return {
            form: {
                serverKey: '',
                allowHttpApi: 'N',
                allowIFrameApi: 'N',
                allowForeignApi: 'N',
                foreignUrl: '',
                urlServerIp: ''
            },
            rules: {
                serverKey: [
                    { required: true, message: '请输入管理级接口密钥', trigger: 'blur' }
                ],
                allowHttpApi: [
                    { required: true, message: '请选择是否开启Http Api', trigger: 'blur' }
                ],
                allowIFrameApi: [
                    { required: true, message: '请选择是否开启IFrame Api', trigger: 'blur' }
                ],
                allowForeignApi: [
                    { required: true, message: '请选择是否开启对外通知 Api', trigger: 'blur' }
                ]
            }
        }
    },
    methods: {
        /**
         * 架载统计数据.
         */
        loadData () {
            //会员统计
            QiRestClient.get('/api/manage/configs' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), (data) => {
                if (!data) {
                    return;
                }
                data.forEach((item) => {
                    this.form[item.configCode] = item.configValue;
                })
            });
        },
        /**
         * 保存配置.
         */
        saveModify () {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    let data = [
                        {configCode: 'serverKey', configValue: this.form.serverKey},
                        {configCode: 'allowHttpApi', configValue: this.form.allowHttpApi},
                        {configCode: 'allowIFrameApi', configValue: this.form.allowIFrameApi},
                        {configCode: 'allowForeignApi', configValue: this.form.allowForeignApi},
                        {configCode: 'foreignUrl', configValue: this.form.foreignUrl},
                        {configCode: 'urlServerIp', configValue: this.form.urlServerIp}
                    ];
                    let success =  (data) => {
                        this.$message.success('操作成功');
                        this.dialogFormVisible = false;
                        this.loadData();
                    };
                    let error = (data) => {
                        this.$message.error(data.message);
                    };
                    QiRestClient.post('/api/manage/configs/actions/setting' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), data, success, error);
                }
            });
        },
        /**
         * 生成接口密钥.
         */
        buildServerKey() {
            this.form.serverKey = QiStringUtils.guid();
        }
    },
    mounted: function () {
        this.loadData();
    }
});