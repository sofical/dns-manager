new Vue({
    el: '#member-register',
    data: function () {
        let validateMobile =  (rule, value, callback) => {
            if (QiStringUtils.isMobile(value)) {
                callback();
            } else {
                callback(new Error(rule.message));
            }
        };

        return {
            registerForm : {
                account: '',
                passwd: '',
                realName: '',
                mobile: '',
                randCode: ''
            },
            rules: {
                account: [
                    { required: true, message: '请输入会员账号', trigger: 'blur' },
                    { min: 4, max: 20, message: '会员账号长度为4-20个字符', trigger: 'blur' }
                ],
                passwd: [
                    { required: true, message: '请输入账号密码', trigger: 'blur' },
                    { min: 6, max: 16, message: '会员账号长度为6-16个字符', trigger: 'blur' }
                ],
                realName: [
                    { required: true, message: '请输入真实姓名', trigger: 'blur' },
                    { min: 1, max: 20, message: '真实姓名长度为1-20个字符', trigger: 'blur' }
                ],
                mobile: [
                    { required: true, message: '请输入手机号码', trigger: 'blur' },
                    { validator: validateMobile, message: '请输入正确的手机号码', trigger: 'blur' }
                ],
                randCode: [
                    { required: true, message: '请输入验证码', trigger: 'blur' },
                    { min: 4, max: 4, message: '请输入正确的验证码', trigger: 'blur' }
                ]
            },
            randCodeUrl: '/members/randcode'
        }
    },
    methods: {
        /**
         * 注册.
         */
        sendRegister () {
            //参数校验.
            this.$refs['registerForm'].validate((valid) => {
                if (valid) {
                    let url = '/api/users';
                    QiRestClient.post(url, this.registerForm,  (data) => {
                        this.$message.success('我们已收到您的注册请求，我们将第一时间为您审核。待审核通过后即可登录，现在为您跳转到首页...');
                        this.registerForm = {
                            account: '',
                            passwd: '',
                            realName: '',
                            mobile: '',
                            randCode: ''
                        };
                        this.refreshCode();
                        setTimeout(function () {
                            location.href="index.html";
                        }, 3000);
                    }, (data) => {
                        this.$message.error(data.message);
                        this.refreshCode();
                    });
                }
            });
        },

        /**
         * 刷新验证码.
         */
        refreshCode () {
            this.randCodeUrl = '/members/randcode?_p=' + (new Date()).getTime();
        }
    }
});