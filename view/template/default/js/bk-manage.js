/**
 * 管理后台.
 * @type {Vue}
 */
let vmManage = new Vue({
    el: '#main-block',
    data: function () {
        return {
            activeIndex: '1',
            menuMap: {
                '1': '/manage-index',
                '2-1': '/manage-domains',
                '2-2': '/manage-records',
                '2-2-1': '/manage-records-balance',
                '3-1': '/manage-network',
                '3-2': '/manage-ipv4',
                '3-3': '/manage-ipv6',
                '3-4': '/manage-config',
                '4': '/manage-hosts',
                '5': '/manage-member'
            }
        }
    },
    methods: {
        /**
         * 加载页面
         * @param menu
         * @param fun
         */
        goToPage: function (menu, fun) {
            $('#mainDiv').tpl(this.menuMap[menu], {}, function () {
                if (fun) {
                    fun();
                }
            });
        },
        /**
         * 采单选择.
         * @param index
         */
        selectMenu: function (index) {
            if (index != '10') {
                location.href = '?menu=' + index + (iFrameToken ? iFrameToken : ('&iframe-token=' + iFrameToken));
            }
            if (index == '10') {
                this.$confirm('确定需要退出管理后台吗？', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(function (){
                    QiRestClient.delete('/api/user/session' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), () => {
                        location.reload();
                    });
                }).catch(function(){
                });
            }
        },
        /**
         * 加载主内容.
         */
        loadBody: function () {
            var index = QiServletUtils.get("menu");
            index = index || '1';
            this.goToPage(index);
            this.activeIndex = index;
        }
    },
    mounted: function () {
        this.loadBody();
    }
});