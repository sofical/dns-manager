<?php
use restphp\tpl\RestTpl;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>会员账号注册-软盛科技</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- css lib start -->
    <?php RestTpl::load('public-css-lib.tpl'); ?>
    <!-- css lib end -->
</head>
<body>
<!-- header section start -->
<div class="header_section">
    <?php RestTpl::load('public-head.tpl'); ?>
</div>
<!-- header section end -->

<!-- contact section start -->
<div class="contact_section layout_padding" id="member-register">
    <div class="container-fluid">
        <div class="contact_text"><b>会员注册</b></div>
        <div class="contact_section2">
            <div class="row">
                <div class="col-md-6 padding_left_0">
                    <div class="map-responsive">
                        <iframe src="" width="600" height="600" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mail_section">

                        <el-row>
                            <el-form :model="registerForm" :rules="rules" ref="registerForm" label-width="100px" class="demo-ruleForm">
                                <el-form-item label="会员账号：" prop="account">
                                    <el-input v-model="registerForm.account" placeholder="请输入您的想注册的账号"></el-input>
                                </el-form-item>
                                <el-form-item label="登录密码：" prop="passwd">
                                    <el-input v-model="registerForm.passwd" placeholder="请输入登录密码" show-password></el-input>
                                </el-form-item>
                                <el-form-item label="真实姓名：" prop="realName">
                                    <el-input v-model="registerForm.realName" placeholder="请输入真实姓名"></el-input>
                                </el-form-item>
                                <el-form-item label="手机号码：" prop="mobile">
                                    <el-input v-model="registerForm.mobile" placeholder="请输入手机号码"></el-input>
                                </el-form-item>
                                <el-form-item label="验证码" prop="randCode">
                                    <el-input v-model="registerForm.randCode" placeholder="请输入验证码" style="width: 310px"></el-input>
                                    <a href="javascript:void(0);" @click="refreshCode">
                                        <img :src="randCodeUrl" class="rs-rand-img" />
                                    </a>
                                </el-form-item>
                            </el-form>
                        </el-row>
                        <div class="send_bt"><a href="javascript:void(0);" title="点击注册新账号" @click="sendRegister">确认注册</a></div>
                        <div class="Locations_text" style="display: flex">
                            <div style="width: 50%;">
                                <i class="el-icon-info"></i><a href="members.html" title="点击进入登录框"><span class="map_icon">已有账号，点击这里去登录</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- contact section end -->
<!-- footer section start -->
<?php RestTpl::load('public-foot.tpl'); ?>
<!--  footer section end -->
<!-- js lib start -->
<?php RestTpl::load('public-js-lib.tpl'); ?>
<!-- js lib end-->

<script src="js/bs-member-register.js"></script>
</body>
</html>