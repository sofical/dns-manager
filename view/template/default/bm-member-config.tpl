<style type="text/css">
    .dns-manager-statistics-domain-title {
        width: 100px;
        background-color: #00b3ee;
        color: #ffffff;
        padding-top: 110px;
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
        border-bottom-left-radius: 20px;
        border-top-left-radius: 20px
    }
    .dns-manager-statistics-record-title {
        width: 100px;
        background-color: #00b3ee;
        color: #ffffff;
        padding-top: 110px;
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
        border-bottom-right-radius: 20px;
        border-top-right-radius: 20px
    }
</style>
<div id="member-config">
    <el-row>
        <el-col :span="2">
            &nbsp;
        </el-col>
        <!-- 域名统计 开始 -->
        <el-col :span="8">
            <el-container style="border-radius: 20px; background-color: #e7e7e7; height: 280px">
                <el-aside class="dns-manager-statistics-domain-title" style="width: 100px">
                    域名<br />
                    (<span v-html="data.domain.normal + data.domain.stop"></span>)
                </el-aside>
                <el-main>
                    <div id="domain-draw" style="height: 230px"></div>
                </el-main>
            </el-container>
        </el-col>
        <!-- 域名统计 结束 -->
        <el-col :span="2">
            &nbsp;
        </el-col>
        <!-- 解析统计 开始 -->
        <el-col :span="8">
            <el-container style="border-radius: 20px; background-color: #e7e7e7; height: 280px">
                <el-main>
                    <div id="record-draw" style="height: 240px"></div>
                </el-main>
                <el-aside class="dns-manager-statistics-record-title" style="width: 100px">
                    解析<br />
                    (<span v-html="data.record.total"></span>)
                </el-aside>
            </el-container>
        </el-col>
        <!-- 解析统计 结束 -->
    </el-row>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script src="js/bm-member-config.js"></script>